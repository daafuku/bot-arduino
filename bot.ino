/*

Arduino Sketch for the Wise Robotics M.A.I.D
This code is responsible for sensor functions, motor control and serial communication.

*/

#include <inttypes.h>

#define PIN_STATUS 13

#define PIN_MOTOR_FL_PWM 5
#define PIN_MOTOR_FL_REV 4
#define PIN_MOTOR_BL_PWM 6
#define PIN_MOTOR_BL_REV 7
#define PIN_MOTOR_FR_PWM 11
#define PIN_MOTOR_FR_REV 12
#define PIN_MOTOR_BR_PWM 10
#define PIN_MOTOR_BR_REV 9

#define PIN_USS_SERVO 3
#define PIN_USS_TRIG 8
#define PIN_USS_ECHO 2
// USS_TIMEOUT is defined as 4 meters
#define USS_TIMEOUT 23200
#define USS_FLAG_CLEAR_NONE 0
#define USS_FLAG_CLEAR_LEFT 1
#define USS_FLAG_CLEAR_RIGHT 2
#define USS_FLAG_CLEAR_BOTH 3
// USS_PROXIMITY_THRESHOLD is defined as 30 centimeters
#define USS_PROXIMITY_THRESHOLD 1740

// position C0 (2) is the centre point
const uint8_t uss_positions[] = {0xC0, 0xD0, 0xE0, 0xD0, 0xC0, 0xB0, 0xA0, 0xB0};
const uint8_t uss_positions_map[] = {2, 1, 0, 1, 2, 3, 4};
unsigned long uss_times[5];
unsigned int uss_flags;

#define STX 0x02
#define ETX 0x03

#define MODE_AUTOMATIC 0
#define MODE_MANUAL 1
#define MODE_TESTING 2

#define CONTROL_SERIAL_TIMEOUT 10
#define CONTROL_ERROR_NONE 0
#define CONTROL_ERROR_NO_DATA 1
#define CONTROL_ERROR_CHECKSUM 2
#define CONTROL_ERROR_TIMEOUT 3
#define CONTROL_ERROR_BAD_LENGTH 4
#define CONTROL_ERROR_NO_STX 5 

// Boot up in automatic mode
uint8_t mode = MODE_AUTOMATIC;

void setup() {
  pinMode(PIN_STATUS, OUTPUT);

  pinMode(PIN_USS_SERVO, OUTPUT);
  pinMode(PIN_USS_TRIG, OUTPUT);
  pinMode(PIN_USS_ECHO, INPUT);

  pinMode(PIN_MOTOR_FL_PWM, OUTPUT);
  pinMode(PIN_MOTOR_FL_REV, OUTPUT);

  pinMode(PIN_MOTOR_FR_PWM, OUTPUT);
  pinMode(PIN_MOTOR_FR_REV, OUTPUT);

  pinMode(PIN_MOTOR_BR_PWM, OUTPUT);
  pinMode(PIN_MOTOR_BR_REV, OUTPUT);

  pinMode(PIN_MOTOR_BL_PWM, OUTPUT);
  pinMode(PIN_MOTOR_BL_REV, OUTPUT);

  Serial.begin(9600);
}

uint8_t control_read_packet(byte* packet) {
  byte in;  

  if(Serial.available() < 1) return CONTROL_ERROR_NO_DATA;

  Serial.readBytes(&in, 1);
  if(in != STX) return CONTROL_ERROR_NO_STX;

  Serial.readBytes(&in, 1);
  if(in > 16) return CONTROL_ERROR_BAD_LENGTH;

  unsigned long start = millis();
  while(Serial.available() < (in + 1)) {
    if(millis() - start > CONTROL_SERIAL_TIMEOUT) {
      return CONTROL_ERROR_TIMEOUT;
    }
  }

  Serial.readBytes(packet, in);

  byte checksum = 0;
  for(int i=0;i<in;i++) {
    checksum ^= packet[i];
  }

  Serial.readBytes(&in, 1);

  if(checksum != in) return CONTROL_ERROR_CHECKSUM;

  return CONTROL_ERROR_NONE;
}

uint8_t control_write_packet(byte* packet, uint8_t length) {
  if(length > 16) {
    return CONTROL_ERROR_BAD_LENGTH;
  }

  byte checksum = STX;

  Serial.write(STX);
  Serial.write(length);
  checksum ^= length;

  for(uint8_t i=0; i<length; i++) {
    Serial.write(packet[i]);
    checksum ^= packet[i];
  }

  Serial.write(checksum);

  return CONTROL_ERROR_NONE;
}

void serial_drain() {
  while(Serial.available()) {
    Serial.read();
  }
}

void helpme() {

  motor_set_speed(0,0);
  mode = MODE_MANUAL;
  
  serial_drain();

  for(int i=0; i < 3; i++) {
    digitalWrite(PIN_MOTOR_FL_REV, LOW);
    tone(PIN_MOTOR_FL_PWM, 330 * 4);
    delay(100);
    noTone(PIN_MOTOR_FL_PWM);
    delay(100);
    digitalWrite(PIN_MOTOR_FR_REV, LOW);
    tone(PIN_MOTOR_FR_PWM, 262 * 4);
    delay(100);
    noTone(PIN_MOTOR_FR_PWM);
    delay(100);
  }
  
  delay(1000);

  digitalWrite(PIN_STATUS, HIGH);
  delay(250);
  digitalWrite(PIN_STATUS, LOW);
  delay(250);
}

void sanity() {
  digitalWrite(PIN_MOTOR_FL_REV, LOW);
  tone(PIN_MOTOR_FL_PWM, 262);
  delay(100);
  noTone(PIN_MOTOR_FL_PWM);

  delay(100);

  digitalWrite(PIN_MOTOR_FR_REV, LOW);
  tone(PIN_MOTOR_FR_PWM, 330);
  delay(100);
  noTone(PIN_MOTOR_FR_PWM);
  
  delay(100);

  digitalWrite(PIN_MOTOR_BL_REV, LOW);
  tone(PIN_MOTOR_BL_PWM, 392);
  delay(100);
  noTone(PIN_MOTOR_BL_PWM);
  
  delay(100);

  digitalWrite(PIN_MOTOR_BR_REV, LOW);
  tone(PIN_MOTOR_BR_PWM, 262 * 2);
  delay(200);
  noTone(PIN_MOTOR_BR_PWM);
  
  delay(100);
}

void scan() {

  uss_flags = USS_FLAG_CLEAR_NONE;

  for(uint8_t i=0; i<9; i++) {
    analogWrite(PIN_USS_SERVO, uss_positions[i % 8]);

    delay(240);

    if(i == 8) {
      digitalWrite(PIN_USS_SERVO, HIGH);
      continue;
    }

    if(i < 2) {
      continue;
    }

    digitalWrite(PIN_USS_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(PIN_USS_TRIG, LOW);
    uss_times[uss_positions_map[i]] = pulseIn(PIN_USS_ECHO, HIGH, USS_TIMEOUT);

  }

  // do some real basic navigation logic here
  if(uss_times[0] > USS_PROXIMITY_THRESHOLD && uss_times[1] > USS_PROXIMITY_THRESHOLD) {
    uss_flags |= USS_FLAG_CLEAR_LEFT;
  }

  if(uss_times[3] > USS_PROXIMITY_THRESHOLD && uss_times[4] > USS_PROXIMITY_THRESHOLD) {
    uss_flags |= USS_FLAG_CLEAR_RIGHT;
  }
}

void motor_set_speed(int8_t left, int8_t right) {
  // unfortunately the default PWM library doesn't support these motors
  // the PWM oscillates at a frequency which is too high

  // uint8_t pwm_left, pwm_right;
  bool rev_left, rev_right;
  bool dig_left, dig_right;
  
  if(left == 0) {
    rev_left = LOW;
    dig_left = LOW;
  }else if(left < 0) {
    rev_left = HIGH;
    dig_left = LOW;
  }else{
    rev_left = LOW;
    dig_left = HIGH;
  }

  if(right == 0) {
    rev_right = LOW;
    dig_right = LOW;
  }else if(right < 0) {
    rev_right = LOW;
    dig_right = HIGH;
  }else{
    rev_right = HIGH;
    dig_right = LOW;
  }

  digitalWrite(PIN_MOTOR_FL_REV, rev_left);
  digitalWrite(PIN_MOTOR_FL_PWM, dig_left);
  // analogWrite(PIN_MOTOR_FL_PWM, pwm_left);

  digitalWrite(PIN_MOTOR_BL_REV, rev_left);
  digitalWrite(PIN_MOTOR_BL_PWM, dig_left);
  // analogWrite(PIN_MOTOR_BL_PWM, pwm_left);

  digitalWrite(PIN_MOTOR_FR_REV, rev_right);
  digitalWrite(PIN_MOTOR_FR_PWM, dig_right);
  // analogWrite(PIN_MOTOR_FR_PWM, pwm_right);

  digitalWrite(PIN_MOTOR_BR_REV, rev_right);
  digitalWrite(PIN_MOTOR_BR_PWM, dig_right);
  // analogWrite(PIN_MOTOR_BR_PWM, pwm_right);
}

void manual() {
  byte packet[16];
  motor_set_speed(0,0);
  
  for(;;) {
    int8_t motor_left_speed, motor_right_speed;

    uint8_t error = 0;
    do {
      error = control_read_packet(packet);
    }while(error == CONTROL_ERROR_NO_STX);

    if(error != CONTROL_ERROR_NONE) continue;

    if(packet[0] == 'C' && packet[1] == 'A') {
      mode = MODE_AUTOMATIC;
      serial_drain();
      return;
    }else if(packet[0] == 'L') {
      motor_left_speed = packet[1];
      motor_set_speed(motor_left_speed, motor_right_speed);
    }else if(packet[0] == 'R') {
      motor_right_speed = packet[1];
      motor_set_speed(motor_left_speed, motor_right_speed);
    }else if(packet[0] == 'U') {
      serial_drain();
      scan();
    }
  }
}

void automatic() {
  byte packet[16];

  motor_set_speed(0,0);

  for(;;) {
    if(control_read_packet(packet) == CONTROL_ERROR_NONE) {
      if(packet[0] == 'C' && packet[1] == 'M') {
	mode = MODE_MANUAL;
	return;
      }
    }

    scan();

    switch(uss_flags & USS_FLAG_CLEAR_BOTH) {
    case USS_FLAG_CLEAR_BOTH: motor_set_speed(127, 127); break;
    case USS_FLAG_CLEAR_LEFT: motor_set_speed(-127, 127); break;
    case USS_FLAG_CLEAR_RIGHT: motor_set_speed(127, -127); break;
    default:
      helpme();
      return;
    }

    delay(600);
    motor_set_speed(0,0);
  }
}

void testing() {
  byte packet[16];
    analogWrite(PIN_USS_SERVO, 0xC0);
    delay(1000);
    // analogWrite(PIN_USS_SERVO, 0xF0);
    // delay(1000);
}

void loop() {
  if(mode != MODE_TESTING) {
    sanity();
  }

  for(;;) {

    if(mode == MODE_AUTOMATIC) {
      automatic();
    }else if(mode == MODE_MANUAL) {
      manual();
    }else if(mode == MODE_TESTING) {
      testing();
    }
  }
}
